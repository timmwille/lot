
## Starterkit >> Modules

Modules are like libraries in other programming languages, that can be import easy in your designs. Just copy module files into /files and add following lines into your .scad file

>include<lot_connectors.scad>
>
>include<lot_utilities.scad>

## lot_connectors.scad

### Connector modules for CNC Cutout and 3D Print Designs

| Name|Example| Image|
|------------|----------|----------|
|dogbone_slot|dogbone_slot(x,y,z,tool_size,roughness)|![](./images/dogbone_slot.png)|
|tbone_slot|tbone_slot(x,y,z,tool_size,roughness)|![](./images/tbone_slot.png) |
|sniglet_slot|sniglet_slot(x,y,z,tool_size,roughness)|![](./images/sniglet_slot.png) |
|dogbone_plug|dogbone_plug(x,y,z,tool_size,roughness)|![](./images/dogbone_plug.png) |
|tbone_plug|tbone_plug(x,y,z,tool_size,roughness)|![](./images/tbone_plug.png) |
|placeholder|placeholder(x,y,z,tool_size)|![](./images/placeholder.png) |

<br>

## lot_utils.scad

### Support modules that facilitates 3D modeling in OpenSCAD

#### Shape modules

|Name|Example|Image|
|-------------|----------|-----------|
|triangular_prism |triangular_prism(p1,p2,p3,height,scale)|![](./images/triangular_prism.png)|
|round_edged_cylinder|round_edged_cylinder(height,radius,edge_radius)|![](./images/round_edged_cylinder_wo_fn.png)|
| |round_edged_cylinder(height,radius,edge_radius,$fn)|![](./images/round_edged_cylinder_w_fn.png)|
