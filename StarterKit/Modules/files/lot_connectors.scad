/*File Info--------------------------------------------------------------------
File Name: lot_connectors.scad
License: Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) 
Name: Jens Meisner
Date: 08/01/20
Desc:  This file is part of the Library of Things, 
    and contains connector modules for CNC and 3D Print designs, 
    e.g. furniture, household items.
Usage: Use "include <filename>" or "use <filename>" to import 
    this module library. 
/*
/*Modifications------------------------------------------------------------
New File Name: Enter name, if it has changed
Name: Enter name of author
Date: 
Desc: Describe changes of the original design
*/
//Please continue with any fur enter any further modification here
//--------------------------------------------------------------------------------


//Uncomment following default variables to make examples below work properly
//tool_size = 6;
//thickness = 12;
//fillet_x = thickness*3;
//fillet_y = thickness;
//fillet_z = thickness;
//roughness = 0.3;


//Connectors For CNC Designs--------------------------------------

//Connection slot "DogBone"
module dogbone_slot(fillet_x,fillet_y,fillet_z,tool_size,roughness)
{
    union()
    {
        translate([0,0,fillet_z/2])
        cube([fillet_x+roughness,fillet_y+roughness,fillet_z],center=true);
        translate([fillet_x/2-tool_size/3+roughness/2,fillet_y/2-tool_size/3+roughness/2,fillet_z/2]) 
        cylinder(h=fillet_z,d=tool_size,center=true,$fn=20);
        translate([-fillet_x/2+tool_size/3-roughness/2,fillet_y/2-tool_size/3+roughness/2,fillet_z/2]) 
        cylinder(h=fillet_z,d=tool_size,center=true,$fn=20);
        translate([fillet_x/2-tool_size/3+roughness/2,-fillet_y/2+tool_size/3-roughness/2,fillet_z/2]) 
        cylinder(h=fillet_z,d=tool_size,center=true,$fn=20);
        translate([-fillet_x/2+tool_size/3-roughness/2,-fillet_y/2+tool_size/3-roughness/2,fillet_z/2]) 
        cylinder(h=fillet_z,d=tool_size,center=true,$fn=20);  
    }
}

//Connection slot "TBone"
module tbone_slot(fillet_x,fillet_y,fillet_z,tool_size,roughness)
{
    union()
    {
        translate([0,0,fillet_z/2])
        cube([fillet_x+roughness,fillet_y+roughness,fillet_z],center=true);
        translate([fillet_x/2-tool_size/2+roughness/2,fillet_y/2+roughness/2,fillet_z/2]) 
        cylinder(h=fillet_z,d=tool_size,center=true,$fn=20);
        translate([-fillet_x/2+tool_size/2-roughness/2,fillet_y/2+roughness/2,fillet_z/2]) 
        cylinder(h=fillet_z,d=tool_size,center=true,$fn=20);
        translate([fillet_x/2-tool_size/2+roughness/2,-fillet_y/2-roughness/2,fillet_z/2]) 
        cylinder(h=fillet_z,d=tool_size,center=true,$fn=20);
        translate([-fillet_x/2+tool_size/2-roughness/2,-fillet_y/2-roughness/2,fillet_z/2]) 
        cylinder(h=fillet_z,d=tool_size,center=true,$fn=20);  
    }
}

//Connection slot "Sniglet"
module sniglet_slot(fillet_x,fillet_y,fillet_z,tool_size,roughness)
{
    difference()
    {
        union()
        {
            translate([0,0,fillet_z/2])
            cube([fillet_x+roughness,fillet_y+roughness,fillet_z],center=true);
            translate([fillet_x/2-tool_size/2+roughness/2,fillet_y/2+tool_size/2+roughness/2,fillet_z/2]) 
            cylinder(h=fillet_z,d=tool_size,center=true,$fn=20);
            translate([-fillet_x/2+tool_size/2-roughness/2,fillet_y/2+tool_size/2+roughness/2,fillet_z/2]) 
            cylinder(h=fillet_z,d=tool_size,center=true,$fn=20);
            translate([fillet_x/2-tool_size/2+roughness/2,-fillet_y/2-tool_size/2-roughness/2,fillet_z/2]) 
            cylinder(h=fillet_z,d=tool_size,center=true,$fn=20);
            translate([-fillet_x/2+tool_size/2-roughness/2,-fillet_y/2-tool_size/2-roughness/2,fillet_z/2]) 
            cylinder(h=fillet_z,d=tool_size,center=true,$fn=20);  
            translate([-fillet_x/2+tool_size/2+tool_size/4-roughness/2,0,fillet_z/2])
            cube([tool_size*1.5,fillet_y+tool_size,fillet_z],center=true);
            translate([fillet_x/2-tool_size/2-tool_size/4+roughness/2,0,fillet_z/2])
            cube([tool_size*1.5,fillet_y+tool_size,fillet_z],center=true);
        }
        union()
        {
            translate([fillet_x/2-tool_size*1.5+roughness/2,fillet_y/2+tool_size/2+roughness/2,fillet_z/2]) 
            cylinder(h=fillet_z,d=tool_size,center=true,$fn=20);
            translate([-fillet_x/2+tool_size*1.5-roughness/2,fillet_y/2+tool_size/2+roughness/2,fillet_z/2]) 
            cylinder(h=fillet_z,d=tool_size,center=true,$fn=20);
            translate([fillet_x/2-tool_size*1.5+roughness/2,-fillet_y/2-tool_size/2-roughness/2,fillet_z/2]) 
            cylinder(h=fillet_z,d=tool_size,center=true,$fn=20);
            translate([-fillet_x/2+tool_size*1.5-roughness/2,-fillet_y/2-tool_size/2-roughness/2,fillet_z/2]) 
            cylinder(h=fillet_z,d=tool_size,center=true,$fn=20);  
        }
    }
}

//Connection plug "DogBone"
module dogbone_plug(fillet_x,fillet_y,fillet_z,tool_size,roughness)
{
    difference()
    {
        union()
        {
            translate([0,fillet_y/2,fillet_z/2])
            cube([fillet_x+(tool_size*4),fillet_y,fillet_z],center=true);
            translate([0,-fillet_y/2,fillet_z/2])
            cube([fillet_x-roughness,fillet_y,fillet_z],center=true);
        }
        translate([fillet_x/2+tool_size/3-roughness/2,-tool_size/3,fillet_z/2]) 
        cylinder(h=fillet_z,d=tool_size,center=true,$fn=20);
        translate([-fillet_x/2-tool_size/3+roughness/2,-tool_size/3,fillet_z/2]) 
        cylinder(h=fillet_z,d=tool_size,center=true,$fn=20);
    }
}

//Connection plug "TBone"
module tbone_plug(fillet_x,fillet_y,fillet_z,tool_size,roughness)
{
    difference()
    {
        union()
        {
            translate([0,fillet_y/2,fillet_z/2])
            cube([fillet_x+(tool_size*4),fillet_y,fillet_z],center=true);
            translate([0,-fillet_y/2,fillet_z/2])
            cube([fillet_x-roughness,fillet_y,fillet_z],center=true);
        }
        translate([fillet_x/2+tool_size/2-roughness/2,0,fillet_z/2]) 
        cylinder(h=fillet_z,d=tool_size,center=true,$fn=20);
        translate([-fillet_x/2-tool_size/2+roughness/2,0,fillet_z/2]) 
        cylinder(h=fillet_z,d=tool_size,center=true,$fn=20);  
    }
}

//Placeholder for "DogBone", "TBone", and "Sniglet". 
//This is used to remove the needed space for plug modules, 
//which will then merge with part via union 
module placeholder(fillet_x,fillet_y,fillet_z,tool_size)
{
    translate([0,fillet_y/2,fillet_z/2])
    cube([fillet_x+(tool_size*4),fillet_y,fillet_z],center=true);
}

//Uncomment module example and press F5/F6 to render
//dogbone_slot(fillet_x,fillet_y,fillet_z,tool_size,roughness
//tbone_slot(fillet_x,fillet_y,fillet_z,tool_size,roughness);
//sniglet_slot(fillet_x,fillet_y,fillet_z,tool_size,roughness);
//dogbone_plug(fillet_x,fillet_y,fillet_z,tool_size,roughness);
//tbone_plug(fillet_x,fillet_y,fillet_z,tool_size,roughness);
//placeholder(fillet_x,fillet_y,fillet_z,tool_size);

