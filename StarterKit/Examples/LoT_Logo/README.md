
## Examples >> LoT_Logo

### Title
StarterKit >> Examples >> LoT_Logo
This is an part of StarterKit >> Examples.
<br>

### Author
Jens Meisner
<br>

### Date
24/11/2020
<br>

### Images
<br>

| Image 1      | Image 2    |
| :------------- | ---------- |
| ![](images/screenshot_256.jpg) |![](images/ph_lot_256.jpg) |
| Image 3      | Image 4    |
| ![![](images/ph_lot_1024.jpg)](images/ph_lot_256.jpg) | ![](images/ph_lot_256.jpg) |

<br>

### 3d Preview
A link to the stl file in the files folder. Future releases of gitea will hopefully integrate an STL 3D Viewer, like Github

<script src="files/LoT_Logo.stl"></script>
<br>

### Description
This is a little logo button with extruded text to be printed via 3D printer
<br>

### BOM
No extra parts
<br>

### Notes
It is not tested as print, it only is used for visualization.
<br>
