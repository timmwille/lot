![CutOuts](CutOuts.png)

## CutOuts
</br>
This designs are created to be cut out via a CNC Machine or Laser Cutter

* **Houses&Sheds** >> Contains elements or modules to build a houses, or house-like structure
* **Interior** >> Includes furniture, and other items inside a house
* **Other** >> Are all designs, that do not match with the other 3 Subcategories, including toys, tools, fashion and other fun stuff
* **Outdoors** >> Things you need in the garden, on the field, outside of the house, or anywhere else.
